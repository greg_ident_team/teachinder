function teracherItemNames() {
  if (document.querySelectorAll(".teacher-item").length) {
    let teachers = document.querySelectorAll(".teacher-item");

    teachers.forEach((e) => {
      const name = e
        .querySelector(".teacher-item--name>p:nth-of-type(1)")
        .textContent.charAt(0);
      const surname = e
        .querySelector(".teacher-item--name>p:nth-of-type(2)")
        .textContent.charAt(0);
      const img = (e.querySelector(".teacher-item--img").dataset.name =
        name + "." + surname);

      addEvents(e);
    });
  } else {
    return;
  }

  function addEvents(element) {
    element.addEventListener("click", openpopupEvent);
  }
  function removeEvent(element) {
    element.removeEventListener("click", openpopupEvent);
  }
  function openpopupEvent(e) {
    e.stopPropagation();
    console.log(e.currentTarget);
  }
}

// Popups

function Popup() {
  this.popup = "";
  this.maincontent = "";
  _self = this;
  this.create = (popupselector, contentselector) => {
    if (popupselector && contentselector) {
      this.popup = document.querySelector(popupselector);
      this.maincontent = document.querySelector(contentselector);
      this.closebtn();
    } else {
      console.warn(
        "You need specify popup HTMl selector and main content HTMl selector"
      );
    }
  };
  this.open = () => {
    this.popup.classList.add("openpopup");
    this.maincontent.classList.add("popupisopen");
    console.log(this.popup, this.maincontent);
  };
  this.close = () => {
    console.log("close", this.popup, this.maincontent);
    this.popup.classList.remove("openpopup");
    this.maincontent.classList.remove("popupisopen");
  };
  this.closebtn = () => {
    console.log(_self.popup);
    if (this.popup.querySelector(".popup-close")) {
      this.popup
        .querySelector(".popup-close")
        .addEventListener("click", (e) => {
          this.close();
        });
    }
  };
}

let popupaddteacher;
let popupteacherinfo;

document.addEventListener("DOMContentLoaded", () => {
  teracherItemNames();
  popupaddteacher = new Popup();
  popupaddteacher.create(".popup-add-teacher", "#app");

  popupteacherinfo = new Popup();
  popupteacherinfo.create(".popup-teacher", "#app");

  // Open popups
  document.querySelectorAll(".jsAddTeacher").forEach((e) => {
    e.addEventListener("click", () => {
      popupaddteacher.open();
    });
  });

  // Just for demo of popup
  document.querySelectorAll(".teacher-item").forEach((e) => {
    e.addEventListener("click", () => {
      popupteacherinfo.open();
    });
  });
});
