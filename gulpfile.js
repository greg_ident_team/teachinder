const { src, dest, watch, series } = require("gulp");
const sass = require("gulp-sass");
const sasssrc = ["scss/*.scss", "scss/*/_*.scss"];
const autopre = require("gulp-autoprefixer");
const sourcemaps = require("gulp-sourcemaps");

// scss to css
function scss() {
  return src(sasssrc)
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(autopre())
    .pipe(sourcemaps.write())
    .pipe(dest("css"));
}

// watch scss
function watchscss() {
  watch(sasssrc, scss);
}
function watchjs() {
  watch(["js/app.js"], runeslint);
}

exports.scsswatch = watchscss;
